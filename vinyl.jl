module vinyl

using Images
using Interpolations

const groove_spacing = 15
const padding = 2 * groove_spacing
const scaling = 8

function xy_to_sample(x, y, width, rps, fsample)
    
    v = [x - width / 2, y - width / 2]
    theta = atan2(v[2], v[1])
    if theta < 0
        theta += 2pi
    end
    r = norm(v)
    groove = (width / 2 - padding - r) / groove_spacing - theta / (2pi)
    if abs(groove - round(groove)) < 0.25
        revolutions = (round(Int, groove) + theta / (2pi))
        time = revolutions / rps
        sample = time * fsample
        # round(Int, sample) + 1
        sample + 1
    else
        0
    end
end

function cut_record(samples, fsample, rpm, width)
    rps = rpm / 60
    
    im = fill(Gray{Float32}(0.5), (width, width)) 
    
    ub = maximum(samples)
    lb = minimum(samples)
    m = median(samples)
    rescale = s -> clamp01nan((s - m) / scaling + 0.5)

    sample_interp = interpolate(samples, BSpline(Cubic(Flat())), OnGrid())
    
    for y in 1:width
        for x in 1:width
            i = xy_to_sample(x, y, width, rps, fsample)
            if i >= 1 && i <= length(samples)
                im[x, y] = rescale(sample_interp[i])
            end
        end
    end
    im
end

function play_record(im, fsample, rpm, padding, groove_spacing, duration=120)
    width = size(im, 1)
    rps = rpm / 60
    nsamples = round(Int, duration * fsample)
    samples = zeros(nsamples)
    for i in 1:nsamples
        time = i / fsample
        revolutions = time * rps
        theta = revolutions * 2 * pi
        r = (width / 2 - padding) - groove_spacing * revolutions
        x = round(Int, width / 2 + r * cos(theta))
        y = round(Int, width / 2 + r * sin(theta))
        samples[i] = Gray(im[x, y]) - 0.5
    end
    samples
    # (samples .- minimum(samples)) ./ (maximum(samples) - minimum(samples))
end

end