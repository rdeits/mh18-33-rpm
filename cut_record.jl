using WAV
using FileIO
using DSP
using Images
using FreeTypeAbstraction
include("vinyl.jl")

const rpm = 33
const width = 4000
const final_rpm = 505.469
const Fs_play = 8000
const groove_spacing = vinyl.groove_spacing
const medley_length = 140
const overlay_length = medley_length * rpm / final_rpm

samples, Fs, nbits, opt  = wavread("audio/combined.wav")
samples = samples[:,1];

im = vinyl.cut_record(samples, Fs, rpm, width)
save("build/medley.png", im)

recovered = vinyl.play_record(im, Fs_play, rpm, 0, groove_spacing, medley_length);
wavwrite(recovered, "build/medley_recovered.wav", Fs=Fs_play)

s2, Fs2, _, _s  = wavread("audio/woodstock_clip.wav")
s2 = s2[:,1];

@show length(samples) / Fs
@show length(s2) * final_rpm / rpm / Fs

im2 = vinyl.cut_record(2 .* s2, Fs2, final_rpm, width);
save("build/woodstock_clip.png", im2)

overlay = convert.(RGB{Float32}, (im .+ im2) .- eltype(im)(0.5))

center = [width / 2 + 0.5, width / 2 + 0.5]
for i in 1:width
    for j in 1:width
        if norm([i, j] - center) >= (width / 2 - vinyl.padding)
            overlay[i, j] = RGB(0., 0., 0.)
        elseif norm([i, j] - center) <= 20
            overlay[i, j] = RGB(0., 0, 0)
        elseif norm([i, j] - center) <= 870
            overlay[i, j] = RGB(0.8, 0.8, 0.1)
        end
    end
end

text = """
    Featured tracks:
        * ? by The Beatles 
        * ? by Bob Dylan 
        * ? by Jethro Tull 
        * ? by The Rolling Stones 
        * ? by The Beatles 
        * ? by Jeff Beck Group 
        * ? by Bob Dylan """

width = 4000
font_array = zeros(UInt8, width, width)
face = newface("unispace/unispace rg.ttf")
lines = split(text, '\n')
scale = 40
row = round(Int, width / 2 - scale * 5)
col = round(Int, width / 2)
for line in lines
    renderstring!(font_array, String(line), face, (scale, scale), row, col, halign=:hleft)
    row += Int(1.5 * scale)
end

overlay .-= Gray.(font_array ./ 255)


save("build/overlay.png", overlay)

wavwrite(vinyl.play_record(load("build/overlay.png"), Fs_play, rpm, 0, groove_spacing, medley_length), "build/overlay_recovered_33.wav", Fs=Fs_play)
wavwrite(vinyl.play_record(load("build/overlay.png"), Fs_play, final_rpm, 0, groove_spacing, overlay_length), "build/overlay_recovered_fast.wav", Fs=Fs_play)
