from __future__ import division

import sys
import PIL.Image as im
import scipy.io.wavfile
import numpy as np


def play_record(filename, rpm, fsample, groove_spacing):
    img = im.open(filename)
    width = img.size[0]
    rps = rpm / 60
    data = []
    for i in range(1000000):
        time = i / fsample
        revolutions = time * rps
        theta = revolutions * 2 * np.pi
        r = width / 2 - groove_spacing * revolutions
        x = int(round(width / 2 + r * np.cos(theta)))
        y = int(round(width / 2 + r * np.sin(theta)))
        data.append(img.getpixel((y - 1, x - 1)))
    data = np.array(data)
    data = 255 * (data - np.min(data)) / (np.max(data) - np.min(data))
    data = np.asarray(data, dtype=np.uint8)
    with open("output.wav", "wb") as wavfile:
        scipy.io.wavfile.write(wavfile, fsample, data)

if __name__ == '__main__':
    filename = sys.argv[1]
    rpm = int(sys.argv[2])
    fsample = int(sys.argv[3])
    groove_spacing = int(sys.argv[4])
    play_record(filename, rpm, fsample, groove_spacing)
